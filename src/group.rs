//! Generalisation of a group type.
use crate::named::Named;
use crate::{intmap::IntMap, maybe_id::*, String};
use serde::{Deserialize, Serialize};
use std::num::NonZeroU32;
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Group<T: MaybeID = ()> {
	#[serde(skip_serializing_if = "is_unit", default)]
	pub id: T,
	pub group_name: String,
	#[serde(default)]
	pub group_website: String,
}
impl<T: IDHasVal> Group<Option<T>> {
	pub fn ok(self) -> Option<Group<T>> {
		let Group {
			id,
			group_name,
			group_website,
		} = self;
		id.map(|id| Group {
			id,
			group_name,
			group_website,
		})
	}
}
impl<T: IDHasVal> WithID<T> for Group<()> {
	type Output = Group<T>;
	fn with_id(self, id: T) -> Group<T> {
		let Group {
			id: (),
			group_name,
			group_website,
		} = self;
		Group {
			id,
			group_name,
			group_website,
		}
	}
}
impl<T: IDHasVal> TakeID for Group<T> {
	type Output = Group<()>;
	type ID = T;
	fn take_id(self) -> (T, Group<()>) {
		let Group {
			id,
			group_name,
			group_website,
		} = self;
		(
			id,
			Group {
				id: (),
				group_name,
				group_website,
			},
		)
	}
	// override because could be a distructive operation otherwise
	fn add_intmap<I>(map: &mut IntMap<Group<()>, T>, iter: I)
	where
		I: IntoIterator<Item = Self>,
	{
		iter
			.into_iter()
			.map(Self::take_id)
			.for_each(move |(id, mut group)| {
				map
					.0
					.entry(id)
					.and_modify(|old| {
						if old.group_website.is_empty()
							&& old.group_website != group.group_website
						{
							// swap out using latest write wins
							core::mem::swap(&mut old.group_website, &mut group.group_website);
						}
					})
					.or_insert(group);
			})
	}
}
impl<T: MaybeID> Group<T> {
	pub fn with_website<S: Into<String>>(&mut self, site: S) -> bool {
		if self.group_website.is_empty() {
			self.group_website = site.into();
			true
		} else {
			false
		}
	}
}
impl<T: MaybeID> From<Named<T>> for Group<T> {
	fn from(Named { id, name }: Named<T>) -> Self {
		Self {
			id,
			group_name: name,
			group_website: String::new(),
		}
	}
}
impl<T: MaybeID> Into<Named<T>> for Group<T> {
	fn into(self) -> Named<T> {
		Named {
			id: self.id,
			name: self.group_name,
		}
	}
}

#[derive(Deserialize, Serialize, Clone, Copy, Debug, PartialEq)]
pub struct GroupIDs {
	pub group_id: u32,
	pub group_id_2: u32,
	pub group_id_3: u32,
}
impl From<GroupIDs> for [Option<std::num::NonZeroU32>; 3] {
	fn from(v: GroupIDs) -> Self {
		let GroupIDs {
			group_id,
			group_id_2,
			group_id_3,
		} = v;
		[
			NonZeroU32::new(group_id),
			NonZeroU32::new(group_id_2),
			NonZeroU32::new(group_id_3),
		]
	}
}
impl From<GroupIDs> for [u32; 3] {
	fn from(v: GroupIDs) -> Self {
		let GroupIDs {
			group_id,
			group_id_2,
			group_id_3,
		} = v;
		[group_id, group_id_2, group_id_3]
	}
}
impl From<[u32; 3]> for GroupIDs {
	fn from([group_id, group_id_2, group_id_3]: [u32; 3]) -> Self {
		Self {
			group_id,
			group_id_2,
			group_id_3,
		}
	}
}
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct GroupNames {
	pub group_name: String,
	pub group_name_2: Option<String>,
	pub group_name_3: Option<String>,
}
impl<T: Into<String>> From<[T; 3]> for GroupNames {
	fn from([group_name, group_name_2, group_name_3]: [T; 3]) -> Self {
		let (group_name, group_name_2, group_name_3) = (
			group_name.into(),
			Some(group_name_2.into()),
			Some(group_name_3.into()),
		);
		Self {
			group_name,
			group_name_2,
			group_name_3,
		}
	}
}
impl<T: From<String>> From<GroupNames> for [T; 3] {
	fn from(v: GroupNames) -> Self {
		let GroupNames {
			group_name,
			group_name_2,
			group_name_3,
		} = v;
		[
			group_name.into(),
			group_name_2.unwrap_or_default().into(),
			group_name_3.unwrap_or_default().into(),
		]
	}
}

pub struct GroupsIter {
	// minor layout optimisation
	index: u32,
	ids: [Option<NonZeroU32>; 3],
	names: [String; 3],
	site0: String,
	// 16 + 4 * 24 => 112
	// 16 + 4 * 12 => 64
}
impl Iterator for GroupsIter {
	type Item = Group<NonZeroU32>;
	fn next(&mut self) -> Option<Self::Item> {
		use std::mem::take;
		loop {
			let (group_name, id, group_website) = match self.index {
				x @ 0..=2 => (
					take(unsafe { self.names.get_unchecked_mut(x as usize) }),
					take(unsafe { self.ids.get_unchecked_mut(x as usize) }),
					take(&mut self.site0),
				),
				_ => return None,
			};
			self.index += 1;
			if let Some(id) = id {
				return Some(Group {
					id,
					group_name,
					group_website,
				});
			}
		}
	}
}

impl GroupNames {
	pub fn with_ids(self, ids: GroupIDs) -> GroupsIter {
		GroupsIter {
			index: 0,
			names: self.into(),
			ids: ids.into(),
			site0: String::new(),
		}
	}
	pub fn with_ids_and_website(
		self,
		ids: GroupIDs,
		site0: String,
	) -> GroupsIter {
		GroupsIter {
			index: 0,
			ids: ids.into(),
			names: self.into(),
			site0,
		}
	}
}
impl GroupIDs {
	pub fn with_names(self, names: GroupNames) -> GroupsIter {
		names.with_ids(self)
	}
	pub fn with_names_and_website(
		self,
		names: GroupNames,
		site0: String,
	) -> GroupsIter {
		names.with_ids_and_website(self, site0)
	}
}
