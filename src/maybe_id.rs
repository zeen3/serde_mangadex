//! ID types
//!
//! In this one, IDHasVal is a subtype of MaybeID.
use crate::intmap::IntMap;
use ::std::num::*;
use std::fmt;
mod seal {
	pub trait Sealed: Copy + 'static {}
	impl Sealed for () {}
	impl Sealed for u32 {}
	impl Sealed for u64 {}
	impl Sealed for ::core::num::NonZeroU32 {}
	impl Sealed for ::core::num::NonZeroU64 {}
}

/// simple means to prevent unit typing
pub fn is_unit(maybe: &dyn core::any::Any) -> bool {
	core::any::Any::is::<()>(maybe)
}

/// A type that might be an ID.
pub trait MaybeID: seal::Sealed {
	type Out: Copy;
	fn id(&self) -> Option<Self::Out> {
		None
	}
	fn as_u64(&self) -> u64;
}
/// A type that has meaning as an ID.
pub trait IDHasVal: Ord + Eq + core::hash::Hash + MaybeID + 'static {
	fn id(&self) -> Self::Out {
		MaybeID::id(self).unwrap()
	}
}

impl<T: IDHasVal> seal::Sealed for Option<T> {}
impl<T: IDHasVal> MaybeID for Option<T> {
	type Out = T;
	fn id(&self) -> Option<Self::Out> {
		*self
	}
	fn as_u64(&self) -> u64 {
		self.map(|v| v.as_u64()).unwrap_or_default()
	}
}

pub struct IDIter<ID: IDHasVal, T: WithID<ID>> {
	iter: ::std::collections::btree_map::IntoIter<ID, T>,
}
impl<ID: IDHasVal, T: WithID<ID>> fmt::Debug for IDIter<ID, T>
where
	ID: fmt::Debug,
	T: fmt::Debug,
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		fmt::Debug::fmt(&self.iter, f)
	}
}
impl<ID: IDHasVal, T: WithID<ID>> Iterator for IDIter<ID, T> {
	type Item = <T as WithID<ID>>::Output;
	fn next(&mut self) -> Option<Self::Item> {
		self.iter.next().map(|(id, val)| val.with_id(id))
	}
}

/// The with-pair of a ID method.
pub trait WithID<T>: Sized
where
	T: IDHasVal,
{
	/// Value after including the ID.
	type Output: TakeID<ID = T, Output = Self>;
	fn with_id(self, id: T) -> Self::Output;
	fn from_intmap(selves: IntMap<Self, T>) -> IDIter<T, Self> {
		IDIter {
			iter: selves.into_iter(),
		}
	}
}
/// The take-pair if a ID taking method.
pub trait TakeID: Sized {
	/// Value after removing the ID.
	type Output: WithID<Self::ID, Output = Self>;
	/// Value of the ID
	type ID: IDHasVal;
	fn take_id(self) -> (Self::ID, Self::Output);
	fn add_intmap<I>(map: &mut IntMap<Self::Output, Self::ID>, selves: I)
	where
		I: IntoIterator<Item = Self>,
	{
		map.extend(selves)
	}
	fn to_intmap<I>(selves: I) -> IntMap<Self::Output, Self::ID>
	where
		I: IntoIterator<Item = Self>,
	{
		selves.into_iter().collect()
	}
}

/// null default type
impl MaybeID for () {
	type Out = ();
	fn as_u64(&self) -> u64 {
		0
	}
}
impl MaybeID for u32 {
	type Out = NonZeroU32;
	fn id(&self) -> Option<Self::Out> {
		NonZeroU32::new(*self)
	}
	fn as_u64(&self) -> u64 {
		*self as u64
	}
}
impl MaybeID for u64 {
	type Out = NonZeroU64;
	fn id(&self) -> Option<Self::Out> {
		NonZeroU64::new(*self)
	}
	fn as_u64(&self) -> u64 {
		*self
	}
}

impl MaybeID for NonZeroU32 {
	type Out = NonZeroU32;
	fn id(&self) -> Option<Self::Out> {
		Some(*self)
	}
	fn as_u64(&self) -> u64 {
		self.get() as u64
	}
}
impl MaybeID for NonZeroU64 {
	type Out = NonZeroU64;
	fn id(&self) -> Option<Self::Out> {
		Some(*self)
	}
	fn as_u64(&self) -> u64 {
		self.get()
	}
}

impl IDHasVal for u32 {}
impl IDHasVal for u64 {}
impl IDHasVal for NonZeroU32 {}
impl IDHasVal for NonZeroU64 {}

#[cfg(feature = "uuid")]
mod uuid {
	use super::{seal::Sealed, IDHasVal, MaybeID, NonZeroU128};
	impl Sealed for u128 {}
	impl Sealed for NonZeroU128 {}
	impl Sealed for uuid::Uuid {}

	impl MaybeID for u128 {
		type Out = NonZeroU128;
		fn id(&self) -> Option<Self::Out> {
			NonZeroU128::new(*self)
		}
		fn as_u64(&self) -> u64 {
			unimplemented!()
		}
	}
	impl MaybeID for NonZeroU128 {
		type Out = NonZeroU128;
		fn id(&self) -> Option<Self::Out> {
			Some(*self)
		}
		fn as_u64(&self) -> u64 {
			unimplemented!()
		}
	}
	impl MaybeID for uuid::Uuid {
		type Out = Self;
		fn id(&self) -> Option<Self::Out> {
			Some(*self)
		}
		fn as_u64(&self) -> u64 {
			unimplemented!()
		}
	}

	impl IDHasVal for NonZeroU128 {}
	impl IDHasVal for u128 {}
	impl IDHasVal for uuid::Uuid {}
}
