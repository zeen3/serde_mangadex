use {
	super::{maybe_id::*, String},
	core::num::NonZeroU64,
	serde::{Deserialize, Serialize},
};
#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Named<ID: MaybeID = NonZeroU64> {
	pub id: ID,
	pub name: String,
}
impl<T: IDHasVal> WithID<T> for Named<()> {
	type Output = Named<T>;
	fn with_id(self, id: T) -> Self::Output {
		let Named { name, id: () } = self;
		Named { id, name }
	}
}
impl<T: IDHasVal> TakeID for Named<T> {
	type Output = Named<()>;
	type ID = T;
	fn take_id(self) -> (T, Named<()>) {
		let Named { name, id } = self;
		(id, Named { name, id: () })
	}
}
