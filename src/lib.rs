#![feature(extended_key_value_attributes)]
#![cfg_attr(feature = "try_trait", feature(try_trait))]
#![cfg_attr(
	feature = "v2-nightly",
	feature(
		min_const_generics,
		maybe_uninit_uninit_array,
		array_methods,
		array_map
	)
)]
//! # Deserialize Mangadex
//!
//! In essence, a wrapper over serde_json for mangadex-specific information.
//! Includes some normalised types.

//! This does not attempt to reproduce output of the original json.
//!
//! Feature `time` is required for use of `chrono` in this module; else all timestamps are
//! of the `u64` type.
//!
//! Feature `display` is required for use of `derive_more` in this module; else all links and
//! further produce only debug types.
//!
//! Feature `smartstring` uses smartstring for reducing use of heap memory.

// setup string smallers
#[cfg(feature = "smartstring")]
extern crate smartstring;
#[cfg(feature = "smartstring")]
pub(crate) use smartstring::alias::String;
#[cfg(all(not(feature = "smartstring"), not(feature = "alloc")))]
pub(crate) use std::string::String;
#[cfg(all(not(feature = "smartstring"), feature = "alloc"))]
extern crate alloc;
#[cfg(all(not(feature = "smartstring"), feature = "alloc"))]
pub(crate) use alloc::string::String;

// clock
#[cfg(feature = "chrono")]
extern crate chrono;
extern crate itoa;
extern crate serde;
extern crate serde_repr;

/// Deserialisable genre types
pub mod genres;
/// Group information
pub mod group;
#[cfg(feature = "intmap")]
/// Module for integer mappings
pub mod intmap;
/// (De)serialisable language types (flag)
pub mod langs;
/// User information
pub mod named;

/// Internal module for IDs
pub mod maybe_id;
/// Internal module that produces a bitwise set
mod set;

#[cfg(feature = "v1")]
pub mod v1;
#[cfg(feature = "v2")]
pub mod v2;
pub use genres::{Genre, GenreSet};
pub use langs::{Language, LanguageSet};
