use crate::{
	genres::*, group::*, intmap::IntMap, maybe_id::*, Language, String,
};
#[cfg(feature = "chrono")]
use chrono::{serde::ts_seconds, DateTime, Utc};
#[cfg(feature = "derive_more")]
use derive_more::Display;
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::{mem, num::NonZeroU32};

/// Information from the mangadex manga api (/api/manga/47; .)
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
#[serde(tag = "status")]
pub enum Manga {
	/// Whether the ID is gone (eg id 420) or doesn't exist (9999999) it makes no distinction.
	#[serde(rename = "Manga ID does not exist.")]
	MangaIDDoesNotExist,
	/// Status is OK, data is fine.
	/// ~~(box because clippy complains, lowercase for lint)~~
	#[serde(rename = "OK")]
	Ok(Data),
}

impl Manga {
	pub fn is_ok(&self) -> bool {
		match self {
			Manga::Ok(_) => true,
			_ => false,
		}
	}
	pub fn does_not_exist(&self) -> bool {
		match self {
			Manga::MangaIDDoesNotExist => true,
			_ => false,
		}
	}
	pub fn ok(self) -> Option<Data> {
		match self {
			Manga::Ok(data) => Some(data),
			_ => None,
		}
	}
}

/// Status of a manga from the mangadex manga api (/api/manga/47; .manga.status)
#[derive(Serialize_repr, Deserialize_repr, Clone, Debug, PartialEq)]
#[cfg_attr(feature = "derive_more", derive(Display))]
#[repr(u8)]
pub enum Status {
	Ongoing = 1,
	Completed = 2,
	Cancelled = 3,
	Hiatus = 4,
	#[serde(other)]
	Unknown = 0,
}

impl From<u8> for Status {
	fn from(v: u8) -> Status {
		if v < 5 {
			unsafe { mem::transmute(v) }
		} else {
			Status::Unknown
		}
	}
}

impl From<Status> for u8 {
	fn from(v: Status) -> u8 {
		v as u8
	}
}

impl Default for Status {
	fn default() -> Self {
		Status::Unknown
	}
}

/// Whether it's for over 18 or not (hentai).
#[derive(
	Debug,
	Deserialize_repr,
	Serialize_repr,
	Clone,
	Copy,
	PartialEq,
	PartialOrd,
	Eq,
	Ord,
)]
#[cfg_attr(feature = "derive_more", derive(Display))]
#[repr(u8)]
pub enum Over18 {
	/// Not over 18 (boolean false).
	No = 0,
	/// Over 18 (boolean true).
	Yes = 1,
}
impl From<bool> for Over18 {
	fn from(b: bool) -> Self {
		if b {
			Over18::Yes
		} else {
			Over18::No
		}
	}
}
impl From<Over18> for bool {
	fn from(b: Over18) -> Self {
		match b {
			Over18::No => false,
			Over18::Yes => true,
		}
	}
}

impl Default for Over18 {
	fn default() -> Self {
		Over18::No
	}
}
#[cfg(feature = "display")]
pub mod links {
	use {
		crate::String,
		derive_more::{Display, From, Into},
		serde::{Deserialize, Serialize},
	};

	/// Bookwalker
	#[derive(
		Deserialize, Serialize, Into, From, Display, Clone, Debug, PartialEq,
	)]
	#[display(fmt = "https://bookwalker.jp/{}", _0)]
	pub struct BookWalker(pub String);
	/// MangaUpdates ID
	#[derive(
		Deserialize, Serialize, Into, From, Display, Clone, Debug, PartialEq,
	)]
	#[display(fmt = "https://www.mangaupdates.com/series.html?id={}", _0)]
	pub struct MangaUpdates(
		#[serde(with = "serde_strz")] pub ::std::num::NonZeroU64,
	);
	/// NovelUpdates slug
	#[derive(
		Deserialize, Serialize, Into, From, Display, Clone, Debug, PartialEq,
	)]
	#[display(fmt = "https://www.novelupdates.com/series/{}/", _0)]
	pub struct NovelUpdates(pub String);
	/// MyAnimeList ID
	#[derive(
		Deserialize, Serialize, Into, From, Display, Clone, Debug, PartialEq,
	)]
	#[display(fmt = "https://myanimelist.net/manga/{}", _0)]
	pub struct MyAnimeList(
		#[serde(with = "serde_strz")] pub ::std::num::NonZeroU64,
	);

	/// MangaAPI Links (/api/manga/47; .manga.links)
	#[derive(Deserialize, Serialize, Default, Clone, Debug, PartialEq)]
	pub struct Links {
		/// Bookwalker
		#[serde(rename(deserialize = "bw"))]
		pub book_walker: Option<BookWalker>,
		/// MangaUpdates ID
		#[serde(rename(deserialize = "mu"))]
		pub manga_updates: Option<MangaUpdates>,
		/// NovelUpdates slug
		#[serde(rename(deserialize = "nu"))]
		pub novel_updates: Option<NovelUpdates>,
		/// Amazon URL
		#[serde(rename(deserialize = "amz"))]
		pub amazon: Option<String>,
		/// CDJapan URL
		#[serde(rename(deserialize = "cdj"))]
		pub cd_japan: Option<String>,
		/// EbookJapan URL
		#[serde(rename(deserialize = "ebj"))]
		pub ebook_japan: Option<String>,
		/// MyAnimeList ID
		#[serde(rename(deserialize = "mal"))]
		pub my_anime_list: Option<MyAnimeList>,
		/// Raw URL
		pub raw: Option<String>,
		/// Official English URL
		#[serde(rename(deserialize = "engtl"))]
		pub english_translation: Option<String>,
	}
}
#[cfg(not(feature = "display"))]
pub mod links {
	use crate::String;
	use serde::{Deserialize, Serialize};
	/// MangaAPI Links (/api/manga/47; .manga.links)
	#[derive(Deserialize, Serialize, Default, Clone, Debug, PartialEq)]
	pub struct Links {
		/// Bookwalker (prefix: https://bookwalker.jp/)
		#[serde(rename = "bw")]
		pub book_walker: Option<String>,
		/// MangaUpdates ID (prefix: https://www.mangaupdates.com/series.html?id=)
		#[serde(rename = "mu")]
		pub manga_updates: Option<String>,
		/// NovelUpdates slug (prefix: https://www.novelupdates.com/series/, postfix: /)
		#[serde(rename = "nu")]
		pub novel_updates: Option<String>,
		/// Amazon URL
		#[serde(rename = "amz")]
		pub amazon: Option<String>,
		/// CDJapan URL
		#[serde(rename = "cdj")]
		pub cd_japan: Option<String>,
		/// EbookJapan URL
		#[serde(rename = "ebj")]
		pub ebook_japan: Option<String>,
		/// MyAnimeList ID (prefix: https://myanimelist.net/manga/)
		#[serde(rename = "mal")]
		pub my_anime_list: Option<String>,
		/// Raw URL
		pub raw: Option<String>,
		/// Official English URL
		#[serde(rename = "engtl")]
		pub english_translation: Option<String>,
	}
}
pub use links::*;

/// Manga information from the mangadex API (/api/manga/47; .manga)
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Info {
	#[cfg(not(feature = "smartstring"))]
	pub cover_url: String,
	#[cfg(feature = "smartstring")]
	pub cover_url: cover::Cover,

	pub description: String,
	pub title: String,
	pub alt_names: Vec<String>,
	pub artist: String,
	pub author: String,
	pub status: Status,
	pub genres: GenreSet,
	pub demographic: Demographic,
	pub last_chapter: String,
	pub last_volume: Option<u32>,
	// TODO: Explain why this is useless
	// pub lang_name: String,
	#[serde(rename(deserialize = "lang_flag"))]
	pub lang: Language,
	pub hentai: Over18,
	pub links: Option<Links>,
	#[cfg(feature = "chrono")]
	#[serde(with = "ts_seconds", default = "Utc::now")]
	pub last_updated: DateTime<Utc>,
	#[cfg(not(feature = "chrono"))]
	pub last_updated: u64,
	pub related: Vec<Related>,
	// yes this is actually nullable, and potentially zero
	pub comments: Option<u32>,
	// haven't seen it go this high but nobody knows
	pub views: u64,
	// highly probable
	pub follows: Option<u32>,
	pub rating: Rating,
}
/// /api/manga/47; .manga.rating
// yes these are all strings. Turning them into compact
// structures is not very reasonable on my phone...
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Rating {
	// eg "8.89"
	pub bayesian: String,
	// eg "8.93"
	pub mean: String,
	// eg "1,865"
	pub users: String,
}
/// Manga relations from the mangadex API (/api/manga/47; .manga.related)
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Related {
	#[serde(rename = "relation_id")]
	pub by: Relation,
	#[serde(rename = "related_manga_id")]
	pub manga_id: u32,
	#[serde(rename = "manga_name")]
	pub title: String,
	#[serde(rename = "manga_hentai")]
	pub over18: Over18,
}
/// Manga relation by how from the mangadex API (/api/manga/47; .manga.related[].relation_id)
#[derive(Deserialize_repr, Serialize_repr, Clone, Copy, Debug, PartialEq)]
#[repr(u8)]
pub enum Relation {
	Prequel = 1,
	Sequel = 2,
	AdaptedFrom = 3,
	SpinOff = 4,
	SideStory = 5,
	MainStory = 6,
	AlternateStory = 7,
	Doujinshi = 8,
	BasedOn = 9,
	Coloured = 10,
	Monochrome = 11,
	SharedUniverse = 12,
	SameFranchise = 13,
	PreSerialization = 14,
	Serialization = 15,
	#[serde(default)]
	Other = 0,
}

/// Chapter information from the information from the mangadex manga api (/api/manga/47; .chapter[])
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Chapter<T: MaybeID = ()> {
	#[serde(skip_serializing_if = "is_unit", default)]
	pub id: T,
	pub volume: String,
	pub chapter: String,
	pub title: String,
	#[serde(rename(deserialize = "lang_code"))]
	pub lang: Language,
	#[serde(flatten)]
	pub group_ids: GroupIDs,
	// TODO: put down a better explanation as to why I'm dropping support of group_name on Chapter
	// pub group_names: GroupNames,
	#[cfg(feature = "chrono")]
	#[serde(with = "ts_seconds", default = "Utc::now")]
	pub timestamp: DateTime<Utc>,
	#[cfg(not(feature = "chrono"))]
	pub timestamp: u64,
	pub comments: Option<u32>,
	#[serde(default)]
	#[cfg(feature = "smartstring")]
	pub covers: Vec<cover::Cover>,
	#[serde(default)]
	#[cfg(not(feature = "smartstring"))]
	pub covers: Vec<String>,
}
#[cfg(feature = "smartstring")]
pub mod cover {
	const PREFIX: &str = "/images/";
	use crate::String;
	use serde::{
		de::{self, Deserializer, Visitor},
		ser::{self, Serializer},
	};
	use std::{convert::Infallible, fmt, str::FromStr};
	/// specialised type to prevent extraneous data
	#[derive(Clone, PartialEq, Eq, Ord, PartialOrd, Hash)]
	pub struct Cover {
		pub cover: String,
	}
	impl FromStr for Cover {
		type Err = Infallible;
		fn from_str(s: &str) -> Result<Self, Infallible> {
			let cover = if let Some(cover) = s.strip_prefix(PREFIX) {
				// fast path
				cover.into()
			} else {
				// very slow path
				let mut cover = String::new();
				cover.push('!');
				cover.push_str(s);
				cover
			};
			Ok(Cover { cover })
		}
	}
	impl fmt::Debug for Cover {
		fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
			f.debug_tuple("Cover").field(&self.cover).finish()
		}
	}
	impl fmt::Display for Cover {
		fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
			if let Some(cover) = self.cover.strip_prefix(PREFIX) {
				f.write_str(cover)
			} else {
				if let Err(e) = f.write_str(PREFIX) {
					return Err(e);
				}
				f.write_str(&self.cover)
			}
		}
	}
	impl<'de> de::Deserialize<'de> for Cover {
		fn deserialize<D: Deserializer<'de>>(d: D) -> Result<Self, D::Error> {
			struct CoverV;
			impl<'de> Visitor<'de> for CoverV {
				type Value = Cover;
				fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
					f.write_str("a string")
				}
				fn visit_str<E: de::Error>(self, s: &str) -> Result<Cover, E> {
					Ok(Cover::from_str(s).unwrap())
				}
			}
			d.deserialize_str(CoverV)
		}
	}
	impl ser::Serialize for Cover {
		fn serialize<S: Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
			s.collect_str(self)
		}
	}
}

impl<T: IDHasVal> WithID<T> for Chapter<()> {
	type Output = Chapter<T>;
	fn with_id(self, id: T) -> Chapter<T> {
		let Chapter {
			id: (),
			volume,
			chapter,
			title,
			lang,
			group_ids,
			// TODO: put down a better explanation as to why I'm dropping support of group_name on Chapter
			// group_names,
			timestamp,
			comments,
			covers,
		} = self;
		Chapter {
			id,
			volume,
			chapter,
			title,
			lang,
			group_ids,
			// TODO: put down a better explanation as to why I'm dropping support of group_name on Chapter
			// group_names,
			timestamp,
			comments,
			covers,
		}
	}
}
impl<T: IDHasVal> TakeID for Chapter<T> {
	type Output = Chapter<()>;
	type ID = T;
	fn take_id(self) -> (T, Chapter<()>) {
		let Chapter {
			id,
			volume,
			chapter,
			title,
			lang,
			group_ids,
			// TODO: put down a better explanation as to why I'm dropping support of group_name on Chapter
			// group_names,
			timestamp,
			comments,
			covers,
		} = self;
		(
			id,
			Chapter {
				id: (),
				volume,
				chapter,
				title,
				lang,
				group_ids,
				// TODO: put down a better explanation as to why I'm dropping support of group_name on Chapter
				// group_names,
				timestamp,
				comments,
				covers,
			},
		)
	}
}

/// Manga data from the mangadex manga api (/api/manga/47; .)
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Data<ID: MaybeID = ()> {
	/// ID of the manga. Defaults to (), using Data<u32> allows default u32
	#[serde(skip_serializing_if = "is_unit", default)]
	pub id: ID,
	/// Manga info
	pub manga: Info,
	/// Chapter IDs info (can be undefined)
	#[serde(rename(deserialize = "chapter"), default)]
	pub chapters: IntMap<Chapter>,
	#[serde(rename(deserialize = "group"), default)]
	pub groups: IntMap<Group>,
}

impl<T: IDHasVal> WithID<T> for Data<()> {
	type Output = Data<T>;
	fn with_id(self, id: T) -> Self::Output {
		let Data {
			id: (),
			manga,
			chapters,
			groups,
		} = self;
		Data {
			id,
			manga,
			chapters,
			groups,
		}
	}
}
impl<T: IDHasVal> TakeID for Data<T> {
	type Output = Data<()>;
	type ID = T;
	fn take_id(self) -> (T, Data<()>) {
		let Data {
			id,
			manga,
			chapters,
			groups,
		} = self;
		(
			id,
			Data {
				id: (),
				manga,
				chapters,
				groups,
			},
		)
	}
}
impl Data<()> {
	pub fn id_from_cover(self) -> Option<Data<NonZeroU32>> {
		let (url, prefix) = {
			#[cfg(feature = "smartstring")]
			{
				(&self.manga.cover_url.cover[..], "manga/")
			}
			#[cfg(not(feature = "smartstring"))]
			{
				(&self.manga.cover_url[..], "/images/manga/")
			}
		};
		if &url[0..prefix.len()] == prefix {
			let cut = &url[prefix.len()..];
			let idx = cut.as_bytes().iter().position(|&b| b == b'.')?;
			(&cut[0..idx]).parse().map(|id| self.with_id(id)).ok()
		} else {
			None
		}
	}
}

#[cfg(test)]
pub mod tests {
	use super::*;
	use serde_json::from_str;
	type R = Result<(), Box<serde_json::Error>>;
	use crate::v1::test_data::*;

	/// Decoding of the test (id 47) manga
	#[test]
	fn it_decodes_the_test_manga() -> Result<(), Box<serde_json::Error>> {
		let test_test_manga: Manga = from_str(TEST_MANGA_OK)?;
		assert!(test_test_manga.is_ok());
		eprintln!("{:?}", test_test_manga);
		let _ = test_test_manga.ok().unwrap();
		Ok(())
	}
	/// Decoding of a deleted (id 420) manga
	#[test]
	fn it_decodes_a_manga_without_data() -> R {
		let test_no_data: Manga = from_str(TEST_MANGA_UNAVAILABLE)?;
		assert!(test_no_data.does_not_exist());
		eprintln!("{:?}", test_no_data);
		Ok(())
	}
	/// Decoding of a large (id 1) manga
	#[test]
	fn it_decodes_a_large_manga() -> R {
		let test_01: Manga = from_str(TEST_MANGA_LARGE)?;
		assert!(test_01.is_ok());
		eprintln!("{:?}", test_01);
		let _ = test_01.ok().unwrap();
		Ok(())
	}
	/// Decoding of another (id 99) manga
	#[test]
	fn it_decodes_another_manga() -> R {
		let test_99: Manga = from_str(TEST_MANGA_OK_2)?;
		assert!(test_99.is_ok());
		eprintln!("{:?}", test_99);
		let _ = test_99.ok().unwrap();
		Ok(())
	}
	/// Decoding of a manga that lacks both chapters and links (id 39000)
	#[test]
	fn it_decodes_a_manga_without_chapters_or_links() -> R {
		let test_39000: Manga = from_str(TEST_MANGA_NO_META_OR_CH)?;
		assert!(test_39000.is_ok());
		eprintln!("{:?}", test_39000);
		let _ = test_39000.ok().unwrap();
		Ok(())
	}
	/// Decoding of a non-manga is an error
	#[test]
	fn it_does_not_decode_a_non_manga() -> Result<(), Manga> {
		let non = r#"{"status":"None"}"#;
		let test_non_manga: Result<Manga, serde_json::Error> = from_str(non);
		if test_non_manga.is_err() {
			let err = test_non_manga.expect_err("Error not found");
			assert!(err.is_data());
			eprintln!("{:?}", err);
			eprintln!("{}", err);
			Ok(())
		} else {
			Err(test_non_manga.unwrap())
		}
	}
}
