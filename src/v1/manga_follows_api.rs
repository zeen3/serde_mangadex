use crate::String;
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
#[serde(untagged)]
pub enum MangaFollowsAPI {
	Error { status: String, message: String },
	Data { result: Vec<FollowsMangaData> },
}
impl MangaFollowsAPI {
	pub fn ok(self) -> Option<Vec<FollowsMangaData>> {
		match self {
			MangaFollowsAPI::Data { result } => Some(result),
			_ => None,
		}
	}
}
#[derive(Debug, Deserialize, Serialize, PartialEq, Clone)]
pub struct FollowsMangaData {
	pub title: String,
	pub manga_id: u32,
	pub follow_type: FollowType,
	pub volume: String,
	pub chapter: String,
	pub rating: Rating,
}
#[derive(Debug, Deserialize_repr, Serialize_repr, PartialEq, Clone, Copy)]
#[repr(u8)]
pub enum FollowType {
	Reading = 1,
	Completed = 2,
	OnHold = 3,
	PlanToRead = 4,
	Dropped = 5,
	ReReading = 6,
	#[serde(default)]
	NotReading = 0,
}
#[repr(u8)]
#[derive(
	Debug, Deserialize_repr, Serialize_repr, Clone, Copy, PartialEq, Eq,
)]
pub enum Rating {
	Unrated = 0,
	Rate1 = 1,
	Rate2 = 2,
	Rate3 = 3,
	Rate4 = 4,
	Rate5 = 5,
	Rate6 = 6,
	Rate7 = 7,
	Rate8 = 8,
	Rate9 = 9,
	Max = 10,
}

#[cfg(test)]
mod test {
	use super::MangaFollowsAPI;
	use crate::v1::test_data::*;
	use serde_json::from_str;
	#[test]
	fn it_decodes_a_minimal_list_of_follows() -> R {
		let ok: MangaFollowsAPI = from_str(TEST_FOLLOWS_OK)?;
		eprintln!("{:?}", ok);
		let err: MangaFollowsAPI = from_str(TEST_FOLLOWS_ERR)?;
		eprintln!("{:?}", err);
		Ok(())
	}
}
