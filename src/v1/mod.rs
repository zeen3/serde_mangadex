//! Mangadex API V1 (/api/?)

/// Direct interaction with the mangadex chapter API
pub mod chapter_api;
/// Direct interaction with the mangadex manga API
pub mod manga_api;
/// Direct interaction with the mangadex manga follows API
pub mod manga_follows_api;

pub use chapter_api::Chapter;
pub use manga_api::Manga;
use serde::{Deserialize, Serialize};

/// API response type to be funneled through.
/// Please don't actually use it.
#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
#[serde(untagged)]
pub enum APIResponse {
	Chapter(Chapter),
	Manga(Manga),
}
impl APIResponse {
	pub fn is_chapter(&self) -> bool {
		match self {
			APIResponse::Chapter(_) => true,
			_ => false,
		}
	}
	pub fn is_manga(&self) -> bool {
		match self {
			APIResponse::Manga(_) => true,
			_ => false,
		}
	}
	pub fn chapter(self) -> Option<Chapter> {
		match self {
			APIResponse::Chapter(ch) => Some(ch),
			_ => None,
		}
	}
	pub fn chapter_ok(self) -> Option<chapter_api::OK> {
		if let APIResponse::Chapter(ch) = self {
			ch.ok()
		} else {
			None
		}
	}
	pub fn manga(self) -> Option<Manga> {
		match self {
			APIResponse::Manga(mn) => Some(mn),
			_ => None,
		}
	}
	pub fn manga_ok(self) -> Option<manga_api::Data> {
		if let APIResponse::Manga(mng) = self {
			mng.ok()
		} else {
			None
		}
	}
}
/// Centralised test data
#[cfg(test)]
pub mod test_data {
	macro_rules! test_data {
        (@SUB: $e:expr) => { () };
        (@CNT: $($e:expr),*) => {
            <[()]>::len(&[$(test_data!(@SUB: $e)),*])
        };
        ($name:ident = $t:ident / $id:literal) => {
            pub const $name: &str = include_str!(
                concat!(
                    "../../testdata/v1/",
                    stringify!($t),
                    "/",
                    stringify!($id),
                    ".json",
                )
            );
        };
        ($name:ident = [$($names:ident,)*]) => {
            pub const $name: [&str; test_data!(@CNT: $($names),*)] = [
                $($names),+
            ];
        };
    }
	pub type R = Result<(), Box<serde_json::Error>>;

	test_data!(TEST_CHAPTER_DELETED = chapter / 2);
	test_data!(TEST_CHAPTER_OK = chapter / 1);
	test_data!(TEST_CHAPTER_OK_2 = chapter / 42);
	test_data!(TEST_CHAPTER_UNAVAILABLE = chapter / 614094);
	test_data!(TEST_CHAPTER_ERROR = chapter / 9999999);
	test_data!(TEST_CHAPTER_DELAYED = chapter / 1067336);
	test_data!(TEST_CHAPTER_EXTERNAL = chapter / 670622);
	test_data!(
		TEST_CHAPTER_ALL_TESTS = [
			TEST_CHAPTER_DELETED,
			TEST_CHAPTER_OK,
			TEST_CHAPTER_OK_2,
			TEST_CHAPTER_UNAVAILABLE,
			TEST_CHAPTER_ERROR,
			TEST_CHAPTER_DELAYED,
			TEST_CHAPTER_EXTERNAL,
		]
	);
	test_data!(TEST_MANGA_OK = manga / 47);
	test_data!(TEST_MANGA_OK_2 = manga / 99);
	test_data!(TEST_MANGA_UNAVAILABLE = manga / 420);
	test_data!(TEST_MANGA_LARGE = manga / 1);
	test_data!(TEST_MANGA_NO_META_OR_CH = manga / 97);
	test_data!(
		TEST_MANGA_ALL_TESTS = [
			TEST_MANGA_OK,
			TEST_MANGA_OK_2,
			TEST_MANGA_UNAVAILABLE,
			TEST_MANGA_LARGE,
			TEST_MANGA_NO_META_OR_CH,
		]
	);
	test_data!(TEST_FOLLOWS_OK = follows / 1);
	test_data!(TEST_FOLLOWS_ERR = follows / 0);
}

/// Generic test suite
#[cfg(test)]
mod api_response {
	use super::test_data::*;
	use super::APIResponse;
	use serde_json::from_str;

	#[test]
	fn it_decodes_a_manga_generically() -> R {
		for mn in &TEST_MANGA_ALL_TESTS {
			let manga: APIResponse = from_str(mn)?;
			assert!(manga.is_manga());
			eprintln!("{:?}", manga);
		}
		Ok(())
	}
	#[test]
	fn it_decodes_a_chapter_generically() -> R {
		for ch in &TEST_CHAPTER_ALL_TESTS {
			let chapter: APIResponse = from_str(ch)?;
			assert!(chapter.is_chapter());
			eprintln!("{:?}", chapter);
		}
		Ok(())
	}
	#[test]
	fn it_decodes_a_manga_generically_and_gives_an_ok_manga() -> R {
		let manga: APIResponse = from_str(TEST_MANGA_OK)?;
		let mng = manga.manga_ok();
		assert!(mng.is_some());
		eprintln!("{:?}", mng);
		Ok(())
	}
	#[test]
	fn it_decodes_a_chapter_generically_and_gives_an_ok_chapter() -> R {
		let chapter: APIResponse = from_str(TEST_CHAPTER_OK)?;
		let ch = chapter.chapter_ok();
		assert!(ch.is_some());
		eprintln!("{:?}", ch);
		Ok(())
	}
}
