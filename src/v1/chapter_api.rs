use crate::{
	group::{Group, GroupIDs, GroupNames, GroupsIter},
	Language, String,
};
#[cfg(feature = "chrono")]
use chrono::{serde::ts_seconds, DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::num::NonZeroU32;

/// Information on chapters that are currently available
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Data {
	/// Server on which it's hosted (/data/ => `https://mangadex.org/data/`)
	pub server: String,
	/// Hash of the data
	pub hash: String,
	/// Array of pages
	pub page_array: Vec<String>,
	/// Server which it's actually hosted
	/// May not be there
	pub server_fallback: Option<String>,
}

#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Groups {
	/// Groups contributing
	#[serde(flatten)]
	pub ids: GroupIDs,
	/// Groups contributing
	#[serde(flatten)]
	pub names: GroupNames,
	/// Group website, if available.
	#[serde(default)]
	pub group_website: String,
}
impl IntoIterator for Groups {
	type IntoIter = GroupsIter;
	type Item = Group<NonZeroU32>;
	fn into_iter(self) -> Self::IntoIter {
		self
			.ids
			.with_names_and_website(self.names, self.group_website)
	}
}

/// Information on a manga chapter
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct ChapterBase<ID = NonZeroU32> {
	pub id: ID,
	#[cfg(feature = "chrono")]
	#[serde(with = "ts_seconds", default = "Utc::now")]
	pub timestamp: DateTime<Utc>,
	#[cfg(not(feature = "chrono"))]
	pub timestamp: u64,
	/// Parent manga (prefix: `https://mangadex.org/api/manga/`)
	pub manga_id: ID,

	pub volume: String,
	pub chapter: String,
	pub title: String,

	#[serde(rename(deserialize = "lang_code"))]
	pub lang: Language,
	pub comments: Option<u32>,
}

/// Chapter API data when delayed
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Delayed {
	/// Information
	#[serde(flatten)]
	pub info: ChapterBase,
	/// Groups contributing
	#[serde(flatten)]
	pub groups: Groups,
}
impl Delayed {
	pub fn groups(&self) -> impl Iterator<Item = Group<NonZeroU32>> {
		self.groups.clone().into_iter()
	}
}
/// Chapter API data when unavailable
/// Note that it does not include the group id.
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Unavailable(ChapterBase);

/// Chapter API data when external
/// Note that it includes more data than is represented here
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct External {
	/// Information
	#[serde(flatten)]
	pub info: ChapterBase,
	/// Contributed external groups
	#[serde(flatten)]
	pub groups: Groups,
	/// External site link
	pub external: String,
}

/// Chapter API data when OK
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct OK {
	/// Pages
	#[serde(flatten)]
	pub pages: Data,
	/// Information
	#[serde(flatten)]
	pub info: ChapterBase,
	/// Contributing groups
	#[serde(flatten)]
	pub groups: Groups,
	pub long_strip: bool,
}

/// Information from the mangadex chapter api (/api/chapter/2; .)
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
#[serde(tag = "status")]
pub enum Chapter {
	/// When the chapter is deleted by the user or a moderator
	#[serde(rename(deserialize = "deleted"))]
	Deleted { id: u32 },
	/// When the chapter has a group delay
	#[serde(rename(deserialize = "delayed"))]
	Delayed(Delayed),
	/// When the chapter is unavailable
	#[serde(rename(deserialize = "unavailable"))]
	Unavailable(Unavailable),
	/// When the chapter is available
	#[serde(rename(deserialize = "OK"))]
	Ok(OK),
	/// When the chapter is an external chapter
	#[serde(rename(deserialize = "external"))]
	External(External),
	/// When some other error happens (eg not created yet)
	#[serde(rename(deserialize = "error"))]
	Error(Error),
}
impl Chapter {
	pub fn is_ok(&self) -> bool {
		match self {
			Chapter::Ok(_) => true,
			_ => false,
		}
	}
	pub fn is_deleted(&self) -> bool {
		match self {
			Chapter::Deleted { .. } => true,
			_ => false,
		}
	}
	pub fn is_delayed(&self) -> bool {
		match self {
			Chapter::Delayed(_) => true,
			_ => false,
		}
	}
	pub fn is_error(&self) -> bool {
		match self {
			Chapter::Error { .. } => true,
			_ => false,
		}
	}
	pub fn is_unavailable(&self) -> bool {
		match self {
			Chapter::Unavailable(_) => true,
			_ => false,
		}
	}
	pub fn is_external(&self) -> bool {
		match self {
			Chapter::External(_) => true,
			_ => false,
		}
	}
	/// Converts a mangadexchapterapi into an Option for information.
	pub fn ok(self) -> Option<OK> {
		match self {
			Chapter::Ok(ok) => Some(ok),
			_ => None,
		}
	}
	pub fn delayed(self) -> Option<Delayed> {
		match self {
			Chapter::Delayed(delayed) => Some(delayed),
			_ => None,
		}
	}
	pub fn err(self) -> Option<Error> {
		match self {
			Chapter::Error(err) => Some(err),
			_ => None,
		}
	}
	pub fn external(self) -> Option<External> {
		match self {
			Chapter::External(ext) => Some(ext),
			_ => None,
		}
	}
}

/// Generic error type
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
pub struct Error {
	pub id: u32,
	pub message: String,
}
use std::fmt;
impl fmt::Display for Error {
	fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
		write!(fmt, "{} on id {}", self.message, self.id)
	}
}
impl std::error::Error for Error {}

#[cfg(test)]
pub mod tests {
	use super::Chapter as API;
	use crate::v1::test_data::*;
	use serde_json::from_str;

	/// Decoding of a deleted chapter
	#[test]
	fn it_decodes_a_deleted_chapter() -> R {
		let test_deleted: API = from_str(TEST_CHAPTER_DELETED)?;
		assert!(test_deleted.is_deleted());
		eprintln!("{:?}", test_deleted);
		Ok(())
	}
	/// Decoding of an available chapter
	#[test]
	fn it_decodes_an_available_chapter() -> R {
		let test_avilable: API = from_str(TEST_CHAPTER_OK)?;
		assert!(test_avilable.is_ok());
		eprintln!("{:?}", test_avilable);
		let _ = test_avilable.ok().unwrap();
		Ok(())
	}
	/// Decoding of another available chapter
	#[test]
	fn it_decodes_another_available_chapter() -> R {
		let test_avilable: API = from_str(TEST_CHAPTER_OK_2)?;
		assert!(test_avilable.is_ok());
		eprintln!("{:?}", test_avilable);
		let _ = test_avilable.ok().unwrap();
		Ok(())
	}
	/// Decoding of an unavailable chapter
	#[test]
	fn it_decodes_an_unavailable_chapter() -> R {
		let test_unavilable: API = from_str(TEST_CHAPTER_UNAVAILABLE)?;
		assert!(test_unavilable.is_unavailable());
		eprintln!("{:?}", test_unavilable);
		Ok(())
	}
	/// Decoding of a delayed chapter
	#[test]
	fn it_decodes_a_delayed_chapter() -> R {
		let test_delayed: API = from_str(TEST_CHAPTER_DELAYED)?;
		assert!(test_delayed.is_delayed());
		eprintln!("{:?}", test_delayed);
		let _ = test_delayed.delayed().unwrap();
		Ok(())
	}
	/// Decoding of an external chapter
	#[test]
	fn it_decodes_an_external_chapter() -> R {
		let test_extern: API = from_str(TEST_CHAPTER_EXTERNAL)?;
		assert!(test_extern.is_external());
		eprintln!("{:?}", test_extern);
		let _ = test_extern.external().unwrap();
		Ok(())
	}
	/// Decoding of a chapter that does not yet exist
	#[test]
	fn it_decodes_a_non_existing_chapter() -> R {
		let test_non_existing: API = from_str(TEST_CHAPTER_ERROR)?;
		assert!(test_non_existing.is_error());
		eprintln!("{:?}", test_non_existing);
		let _ = test_non_existing.err().unwrap();
		Ok(())
	}
	/// Decoding of a non-chapter is an error
	#[test]
	fn it_does_not_decode_a_non_chapter() -> Result<(), API> {
		let test_non_chapter: Result<API, serde_json::Error> =
			from_str("{\"status\":\"None\"}");
		if test_non_chapter.is_err() {
			let err = test_non_chapter.expect_err("Error not found");
			assert!(err.is_data());
			eprintln!("{:?}", err);
			eprintln!("{}", err);
			Ok(())
		} else {
			Err(test_non_chapter.unwrap())
		}
	}
}
