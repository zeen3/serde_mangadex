#[cfg(test)]
mod test {
	use super::*;
	use crate::v2::test_data::*;
	#[test]
	fn many() -> R {
		TEST_GROUPS.iter().try_for_each(|s| {
			let group: Res<Group> = serde_json::from_str(s)?;
			let _: Result<_, _> = dbg!(group.into());
			Ok(())
		})
	}
	#[test]
	fn one() -> R {
		let group: Res<Group> = serde_json::from_str(TEST_GROUP_DOKI)?;
		let ok: Result<_, _> = group.into();
		let _ = dbg!(ok)?;
		Ok(())
	}
	#[test]
	#[should_panic]
	fn group0() {
		let group: Res<Group> = serde_json::from_str(TEST_GROUP_0).unwrap();
		let ok: Result<_, _> = group.into();
		let _ = dbg!(ok).unwrap();
	}
}

use crate::{
	intmap::IntMap, langs::Language, maybe_id::*, named::Named, String,
};
use core::num::NonZeroU64;
use serde::*;

// #[serde(untagged)]
// pub enum AltNames{
//     Str(String),
//     Arr(Vec<String>)
// }
#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Group<ID: MaybeID = NonZeroU64> {
	pub id: ID,
	pub name: String,
	pub alt_names: String,
	pub language: Language,
	pub leader: Named,
	pub members: Vec<Named>,
	pub description: String,
	pub website: String,
	pub discord: String,
	pub irc_server: String,
	pub irc_channel: String,
	pub email: String,
	#[cfg(feature = "chrono")]
	pub founded: chrono::NaiveDate,
	#[cfg(not(feature = "chrono"))]
	pub founded: String,
	pub likes: u32,
	pub follows: u32,
	pub views: u64,
	pub chapters: u32,
	pub thread_id: u64,
	pub thread_posts: u32,
	pub is_locked: bool,
	pub is_inactive: bool,
	pub delay: u32,
	#[cfg(feature = "chrono")]
	#[serde(with = "chrono::serde::ts_seconds", default = "chrono::Utc::now")]
	pub last_updated: chrono::DateTime<chrono::Utc>,
	#[cfg(not(feature = "chrono"))]
	pub last_updated: u64,
	pub banner: Option<String>,
	#[cfg(test)]
	#[serde(flatten)]
	pub _extra: super::test_data::Ex,
}
impl<ID: MaybeID> Into<crate::group::Group<ID>> for Group<ID> {
	fn into(self) -> crate::group::Group<ID> {
		let Self {
			id,
			name: group_name,
			website: group_website,
			..
		} = self;
		crate::group::Group {
			id,
			group_name,
			group_website,
		}
	}
}
impl<T: IDHasVal> WithID<T> for Group<()> {
	type Output = Group<T>;
	fn with_id(self, id: T) -> Self::Output {
		let Self {
			id: (),
			name,
			alt_names,
			language,
			leader,
			members,
			description,
			website,
			discord,
			irc_server,
			irc_channel,
			email,
			founded,
			likes,
			follows,
			views,
			chapters,
			thread_id,
			thread_posts,
			is_locked,
			is_inactive,
			delay,
			last_updated,
			banner,
			#[cfg(test)]
			_extra,
		} = self;
		Group {
			id,
			name,
			alt_names,
			language,
			leader,
			members,
			description,
			website,
			discord,
			irc_server,
			irc_channel,
			email,
			founded,
			likes,
			follows,
			views,
			chapters,
			thread_id,
			thread_posts,
			is_locked,
			is_inactive,
			delay,
			last_updated,
			banner,
			#[cfg(test)]
			_extra,
		}
	}
}
impl<ID: IDHasVal> TakeID for Group<ID> {
	type ID = ID;
	type Output = Group<()>;
	fn take_id(self) -> (ID, Group<()>) {
		let Self {
			id,
			name,
			alt_names,
			language,
			leader,
			members,
			description,
			website,
			discord,
			irc_server,
			irc_channel,
			email,
			founded,
			likes,
			follows,
			views,
			chapters,
			thread_id,
			thread_posts,
			is_locked,
			is_inactive,
			delay,
			last_updated,
			banner,
			#[cfg(test)]
			_extra,
		} = self;
		(
			id,
			Group {
				id: (),
				name,
				alt_names,
				language,
				leader,
				members,
				description,
				website,
				discord,
				irc_server,
				irc_channel,
				email,
				founded,
				likes,
				follows,
				views,
				chapters,
				thread_id,
				thread_posts,
				is_locked,
				is_inactive,
				delay,
				last_updated,
				banner,
				#[cfg(test)]
				_extra,
			},
		)
	}
	// override because could be a destructive operation otherwise
	fn add_intmap<I>(map: &mut IntMap<Group<()>, ID>, iter: I)
	where
		I: IntoIterator<Item = Self>,
	{
		iter
			.into_iter()
			.map(Self::take_id)
			.for_each(move |(id, mut group)| {
				map
					.0
					.entry(id)
					.and_modify(|old| {
						if old.last_updated <= group.last_updated {
							// swap out using latest wins
							core::mem::swap(old, &mut group);
						}
					})
					.or_insert(group);
			})
	}
}
