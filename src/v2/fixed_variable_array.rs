use {
	core::{fmt, mem},
	serde::{
		de::{Deserializer, Error, SeqAccess, Visitor},
		ser::Serializer,
		Deserialize, Serialize,
	},
};
struct TooManyElements<const N: usize> {
	max: [(); N],
}
impl<const N: usize> fmt::Debug for TooManyElements<N> {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.debug_struct("TooManyElements").field("max", &N).finish()
	}
}
impl<const N: usize> fmt::Display for TooManyElements<N> {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.write_str("array with maximum ")
			.and_then(|()| fmt::Display::fmt(&N, f))
			.and_then(|()| f.write_str(" items"))
	}
}
impl<const N: usize> std::error::Error for TooManyElements<N> {}
struct MLV<T, const N: usize> {
	arr: [Option<T>; N],
	idx: usize,
}
impl<T, const N: usize> MLV<T, N> {
	pub fn new() -> Self {
		MLV {
			idx: 0,
			arr: [None; N],
		}
	}
}
impl<'de, T: Deserialize<'de>, const N: usize> Visitor<'de> for MLV<T, N> {
	type Value = MaxLArr<T, N>;
	fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
		fmt::Display::fmt(&TooManyElements { max: [(); N] }, f)
	}
	fn visit_seq<S: SeqAccess<'de>>(
		mut self,
		mut s: S,
	) -> Result<Self::Value, S::Error> {
		while self.idx < N {
			unsafe {
				*self.arr.get_unchecked_mut(self.idx) = s.next_element()?;
			}
			self.idx += 1;
		}
		let post = s.next_element::<T>();
		match post {
			Ok(Some(_)) => Err(S::Error::custom(TooManyElements { max: [(); N] })),
			Ok(None) => Ok(MaxLArr(self.arr)),
			Err(e) => Err(e), // I thought I had a better way
		}
	}
}
/// Internal type.
#[derive(Debug, Clone, Copy)]
#[cfg_attr(
	feature = "derive_more",
	derive(derive_more::Deref, derive_more::DerefMut)
)]
pub struct MaxLArr<T, const N: usize>([Option<T>; N]);
impl<'de, T, const N: usize> Deserialize<'de> for MaxLArr<T, N>
where
	T: Deserialize<'de>,
{
	fn deserialize<D>(d: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		// impl<T, const N: usize> Drop for MLV<T, N> {
		// 	fn drop(&mut self) {
		// 		let Self { idx, mut arr } = self;
		// 		let slic = arr.as_mut_slice();
		// 		let (ok, _) = slic.split_at_mut(*idx);
		// 		for i in 0..core::cmp::max(N, *idx) {
		// 			core::ptr::drop_in_place(ok.get_unchecked_mut(i));
		// 		}
		// 	}
		// }
		d.deserialize_seq(MLV::new())
	}
}
impl<T: Serialize, const N: usize> Serialize for MaxLArr<T, N> {
	fn serialize<S: Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
		s.collect_seq(self.into_iter())
	}
}
pub struct IntoIter<T, const N: usize> {
	inner: [mem::MaybeUninit<Option<T>>; N],
	idx: usize,
}
impl<T, const N: usize> Iterator for IntoIter<T, N> {
	type Item = T;
	fn next(&mut self) -> Option<Self::Item> {
		loop {
			if self.idx == N {
				return None;
			} else {
				let val =
					unsafe { self.inner.get_unchecked_mut(self.idx).assume_init() };
				self.idx += 1;
				match val.take() {
					Some(v) => return Some(v),
					_ => (),
				}
			}
		}
	}
}

impl<T, const N: usize> IntoIterator for MaxLArr<T, N> {
	type Item = T;
	type IntoIter = IntoIter<T, N>;
	fn into_iter(self) -> Self::IntoIter {
		IntoIter {
			inner: self.0.map(mem::MaybeUninit::new),
			idx: 0,
		}
	}
}

pub struct RefIter<'a, T, const N: usize> {
	inner: &'a [Option<T>; N],
	idx: usize,
}
impl<'a, T, const N: usize> Iterator for RefIter<'a, T, N> {
	type Item = &'a T;
	fn next(&mut self) -> Option<Self::Item> {
		loop {
			if self.idx == N {
				return None;
			} else {
				let val = unsafe { self.inner.get_unchecked(self.idx) };
				self.idx += 1;
				match val {
					Some(ref t) => return Some(t),
					_ => (),
				}
			}
		}
	}
}
impl<'a, T, const N: usize> IntoIterator for &'a MaxLArr<T, N> {
	type Item = &'a T;
	type IntoIter = RefIter<'a, T, N>;
	fn into_iter(self) -> Self::IntoIter {
		RefIter {
			inner: &self.0,
			idx: 0,
		}
	}
}

pub struct IterMut<'a, T, const N: usize> {
	inner: &'a mut [Option<T>; N],
	idx: usize,
}
impl<'a, T, const N: usize> Iterator for IterMut<'a, T, N> {
	type Item = &'a mut T;
	fn next(&mut self) -> Option<Self::Item> {
		loop {
			if self.idx == N {
				return None;
			} else {
				let val = unsafe { self.inner.get_unchecked_mut(self.idx) };
				self.idx += 1;
				match val {
					Some(mut t) => return Some(&mut t),
					_ => (),
				}
			}
		}
	}
}
impl<'a, T, const N: usize> IntoIterator for &'a mut MaxLArr<T, N> {
	type Item = &'a mut T;
	type IntoIter = IterMut<'a, T, N>;
	fn into_iter(self) -> Self::IntoIter {
		IterMut {
			inner: &mut self.0,
			idx: 0,
		}
	}
}
