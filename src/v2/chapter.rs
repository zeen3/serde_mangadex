use crate::{langs::*, maybe_id::*, named::*, String};
use serde::*;
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Available<ID: IDHasVal, MID: IDHasVal> {
	pub id: ID,
	#[serde(with = "hex::serde")]
	pub hash: [u8; 16],
	pub manga_id: MID,
	pub manga_title: String,
	pub volume: String,
	pub chapter: String,
	pub title: String,
	pub language: Language,
	// #[cfg(feature = "v2-nightly")]
	// #[serde(default)]
	// pub groups: super::fixed_variable_array::MaxLArr<Named, 3>,
	#[cfg(not(feature = "v2-nightly"))]
	#[serde(default)]
	pub groups: Vec<Named>,
	pub uploader: u64,
	#[cfg(feature = "chrono")]
	#[serde(with = "chrono::serde::ts_seconds", default = "chrono::Utc::now")]
	pub timestamp: chrono::DateTime<chrono::Utc>,
	#[cfg(not(feature = "chrono"))]
	pub timestamp: u64,
	pub comments: u64,
	pub status: String,
	pub pages: Vec<String>,
	pub server: String,
	/// note: empty if fallback is unneeded
	#[serde(default)]
	pub server_fallback: String,
}
#[cfg(test)]
mod tests {}
