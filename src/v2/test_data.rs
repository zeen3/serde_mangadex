pub type Ex = ::std::collections::HashMap<String, serde_json::Value>;
pub type R = Result<(), Box<dyn ::std::error::Error>>;
pub type Res<T> = super::Response<T>;

macro_rules! test_data {
    (@SUB: $e:expr) => { () };
    (@CNT: $($e:expr),*) => {
        <[()]>::len(&[$(test_data!(@SUB: $e)),*])
    };
    ($name:ident = $t:ident / [$($id:literal,)+]) => {
        test_data!($name = [$(test_data!($t / $id),)+]);
    };
    ($name:ident = [$($data:expr,)*]) => {
        pub(crate) const $name: [&str; test_data!(@CNT: $($data),*)] = [
            $($data),+
        ];
    };
    ($name:ident = $t:ident / $id:literal) => {
        pub(crate) const $name: &str = test_data!($t / $id);
    };
    ($t:ident / $id:literal) => {
        include_str!(
            concat!(
                "../../testdata/v2/",
                stringify!($t),
                "/",
                stringify!($id),
                ".json",
            )
        );
    };
}

test_data!(TEST_INDEX = index / 0);
test_data!(TEST_GROUP_0 = group / 0);
test_data!(TEST_GROUP_DOKI = group / 1);
test_data!(TEST_GROUP_2 = group / 2);
test_data!(
	TEST_GROUPS = group
		/ [
			0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
		]
);

test_data!(TEST_USER_0 = user / 0);
test_data!(TEST_USER_MANGADEX = user / 1);
test_data!(TEST_USER_HOLO = user / 2);
test_data!(
	TEST_USERS = user
		/ [
			0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
			21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
			39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
			57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
			75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
			93, 94, 95, 96, 97, 98, 99, 100,
		]
);
