use std::collections::BTreeMap as Map;
use {
	crate::String,
	serde::{Deserialize, Serialize},
};
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Index {
	pub information: String,
	pub base_url: String,
	pub resources: Map<String, Resource>,
}
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Resource {
	pub description: String,
	#[serde(default)]
	pub aliases: Vec<String>,
	#[serde(default)]
	pub path_parameters: Map<String, String>,
	#[serde(default)]
	pub query_parameters: Map<String, String>,
	#[serde(default)]
	pub json_body_parameters: Map<String, String>,
	#[serde(default)]
	pub sub_resources: Map<String, Resource>,
	#[cfg(test)]
	#[serde(flatten)]
	pub _extra: super::test_data::Ex,
}
pub type IndexResp = super::Response<Index>;
#[test]
fn index() -> crate::v2::test_data::R {
	let index = crate::v2::test_data::TEST_INDEX;
	let index: IndexResp = serde_json::from_str(index)?;
	let _: Result<_, _> = dbg!(index.into());
	Ok(())
}
