#[cfg(test)]
mod test {
	use super::*;
	pub(crate) use crate::v2::test_data::*;
	#[test]
	fn many() -> R {
		TEST_USERS.iter().try_for_each(|s| {
			let group: Res<User> = serde_json::from_str(s)?;
			let _: Result<_, _> = dbg!(group.into());
			Ok(())
		})
	}
	#[test]
	fn holo() -> R {
		let user: Res<User> = serde_json::from_str(TEST_USER_HOLO)?;
		let ok: Result<_, _> = user.into();
		let _ = dbg!(ok)?;
		Ok(())
	}
	#[test]
	#[should_panic]
	fn user0() {
		let user: Res<User> = serde_json::from_str(TEST_USER_0).unwrap();
		let ok: Result<_, _> = user.into();
		let _ = dbg!(ok).unwrap();
	}
}

use crate::{maybe_id::*, named::Named, String};
use core::num::NonZeroU64;
use serde::*;
use serde_repr::*;

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum LevelID {
	Banned = 0,
	#[serde(default)]
	Guest = 1,
	Validating = 2,
	Member = 3,
	Contributor = 4,
	GroupLeader = 5,
	PowerUploader = 6,
	VIP = 9,
	PublicRelations = 10,
	ForumModerator = 11,
	Moderator = 12,
	Developer = 15,
	Administrator = 16,
	Administrator2 = 20,
}
#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum MangadexAtHome {
	Nope = 0,
	Normal = 1,
	EarlyAdopter = 2,
}
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct User<ID: MaybeID = NonZeroU64> {
	pub id: ID,
	pub username: String,
	pub level_id: LevelID,
	#[cfg(feature = "chrono")]
	#[serde(with = "chrono::serde::ts_seconds", default = "chrono::Utc::now")]
	pub joined: chrono::DateTime<chrono::Utc>,
	#[cfg(not(feature = "chrono"))]
	pub joined: u64,
	#[cfg(feature = "chrono")]
	#[serde(with = "chrono::serde::ts_seconds", default = "chrono::Utc::now")]
	pub last_seen: chrono::DateTime<chrono::Utc>,
	#[cfg(not(feature = "chrono"))]
	pub last_seen: u64,
	pub website: String,
	pub biography: String,
	pub views: u64,
	pub uploads: u32,
	pub premium: bool,
	pub md_at_home: MangadexAtHome,
	pub avatar: Option<String>,
	#[cfg(test)]
	#[serde(flatten)]
	_extra: test::Ex,
}
impl<ID: IDHasVal> WithID<ID> for User<()> {
	type Output = User<ID>;
	fn with_id(self, id: ID) -> User<ID> {
		let Self {
			id: (),
			username,
			level_id,
			joined,
			last_seen,
			website,
			biography,
			views,
			uploads,
			premium,
			md_at_home,
			avatar,
			#[cfg(test)]
			_extra,
		} = self;
		User {
			id,
			username,
			level_id,
			joined,
			last_seen,
			website,
			biography,
			views,
			uploads,
			premium,
			md_at_home,
			avatar,
			#[cfg(test)]
			_extra,
		}
	}
}
impl<ID: IDHasVal> TakeID for User<ID> {
	type ID = ID;
	type Output = User<()>;
	fn take_id(self) -> (ID, User<()>) {
		let Self {
			id,
			username,
			level_id,
			joined,
			last_seen,
			website,
			biography,
			views,
			uploads,
			premium,
			md_at_home,
			avatar,
			#[cfg(test)]
			_extra,
		} = self;
		(
			id,
			User {
				id: (),
				username,
				level_id,
				joined,
				last_seen,
				website,
				biography,
				views,
				uploads,
				premium,
				md_at_home,
				avatar,
				#[cfg(test)]
				_extra,
			},
		)
	}
}
