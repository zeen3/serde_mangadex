use crate::String;
use core::fmt;
use core::num::NonZeroU16;
use serde::{Deserialize, Serialize};
/// Response type wrapping all V2 APIs.
#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "status")]
pub enum Response<T> {
	#[serde(rename = "OK")]
	Ok { code: NonZeroU16, data: T },
	#[serde(rename = "error")]
	Err { code: NonZeroU16, message: String },
}
const OK: NonZeroU16 = unsafe { NonZeroU16::new_unchecked(200) };
#[derive(Serialize, Deserialize, Debug)]
pub struct Error {
	code: NonZeroU16,
	message: String,
}
impl ::std::error::Error for Error {}
impl fmt::Display for Error {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{} (code {})", self.message, self.code)
	}
}
impl<T> Into<Result<T, Error>> for Response<T> {
	fn into(self) -> Result<T, Error> {
		match self {
			Response::Ok { code: _, data } => Ok(data),
			Response::Err { code, message } => Err(Error { code, message }),
		}
	}
}
impl<T> From<Result<T, Error>> for Response<T> {
	fn from(res: Result<T, Error>) -> Self {
		match res {
			Ok(data) => Response::Ok { code: OK, data },
			Err(Error { code, message }) => Response::Err { code, message },
		}
	}
}
#[cfg(feature = "try_trait")]
impl<T> core::ops::Try for Response<T> {
	type Ok = T;
	type Error = Error;
	fn into_result(self) -> Result<T, Error> {
		self.into()
	}
	fn from_error(Error { code, message }: Error) -> Self {
		Response::Err { code, message }
	}
	fn from_ok(data: T) -> Self {
		Response::Ok { data, code: OK }
	}
}

pub mod chapter;
pub mod group;
#[cfg(feature = "v2-includes")]
pub mod include_queries;
#[cfg(feature = "v2-index")]
pub mod index;
#[cfg(test)]
pub mod test_data;
pub mod user;

#[cfg(feature = "v2-nightly")]
pub mod fixed_variable_array;
