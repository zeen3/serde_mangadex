use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct UserIncludeChapters {
	pub user: User,
	pub chapters: Vec<Chapter>,
	pub groups: Vec<Named>,
}
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct TitleIncludeChapters {
	#[serde(alias = "title")]
	pub manga: Manga,
	pub chapters: Vec<Chapter>,
	pub groups: Vec<Named>,
}
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct GroupIncludeChapters {
	pub group: Group,
	pub chapters: Vec<Chapter>,
	pub groups: Vec<Named>,
}
