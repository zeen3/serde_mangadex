#![allow(deprecated)]
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::{fmt, mem};

const NUM_GENRES: u8 = 84;

macro_rules! genres {
	( $( $( #[$meta:meta] )* $id:literal => $Genre:ident $( ($title:literal) )? / $Category:ident, )*) => {
		/// Genres supported by MangaDex.
		///
		/// [Category]: ./enum.Category.html
		/// [Demographic]: ./enum.Demographic.html
		#[derive(Deserialize_repr, Serialize_repr, Eq, Ord, PartialEq, PartialOrd, Clone, Copy, Debug)]
		#[cfg_attr(feature = "derive_more", derive(derive_more::Display))]
		#[repr(u8)]
        #[non_exhaustive]
		pub enum Genre {
			$(
				$( #[$meta] )*
				#[doc = concat!("\n\nGenre ", stringify!($Genre), ", ID of ", $id, ", and [categorised][Category] as [", stringify!($Category), "](./enum.Category.html#variant.", stringify!($Category), ").")]
				$( #[doc = concat!(" Has other title: ", $title)] )*

				$( #[cfg_attr(feature = "derive_more", display(fmt = $title))] )?
				$Genre = $id,
			)*
			#[serde(other)]
			/// Some other genre, that's not known by the deserialiser. If this occurs, please report it.
			Unknown = 0,
		}
		impl Default for Genre { fn default() -> Self { 0u8.into() } }
        impl Genre {
            pub fn category(&self) -> Category {
                match self {
                    $(Genre::$Genre => Category::$Category,)*
                        Genre::Unknown => Category::Unknown
                }
            }
        }
	};
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
#[cfg_attr(feature = "derive_more", derive(derive_more::Display))]
#[non_exhaustive]
pub enum Category {
	/// An unknown category. Shouldn't be used, but is returned from the category request for unknown
	/// categories.
	Unknown,
	/// Content warnings, such as sexual themes, gore, and sexual violence.
	Content,
	/// Content format or details, such as derivative work, long strip, or disconnected stories (anthology).
	Format,
	/// Content genres or information, including action, drama, isekai, wuxia, and yuri.
	Genre,
	/// Content themes or contents, including crossdressing, cooking, harem, military, and video games.
	Theme,
	#[deprecated]
	/// Content demographic, deprecated. Use [Demographic][] instead.
	Demographic,
}

genres! {
	1 => FourKoma("4-Koma") / Format,
	2 => Action / Genre,
	3 => Adventure / Genre,
	4 => AwardWinning("Award Winning") / Format,
	5 => Comedy / Genre,
	6 => Cooking / Theme,
	7 => Doujinshi / Format,
	8 => Drama / Genre,
	9 => Ecchi / Content,
	10 => Fantasy / Genre,
	11 => Gyaru / Theme,
	12 => Harem / Theme,
	13 => Historical / Genre,
	14 => Horror / Genre,
	#[deprecated]
	/// Duplicated by [Demographic][].
	15 => ShounenDemographic("Shounen") / Demographic,
	16 => MartialArts("Martial Arts") / Theme,
	17 => Mecha / Genre,
	18 => Medical / Genre,
	19 => Music / Theme,
	20 => Mystery / Genre,
	21 => Oneshot / Format,
	22 => Psychological / Genre,
	23 => Romance / Genre,
	24 => SchoolLife("School Life") / Theme,
	25 => SciFi("Sci-Fi") / Genre,
	#[deprecated]
	/// Duplicated by [Demographic][].
	26 => ShoujoDemographic("Shoujo") / Demographic,
	#[deprecated]
	/// Duplicated by [Demographic][].
	27 => SeinenDemographic("Seinen") / Demographic,
	28 => ShoujoAi("Shoujo Ai") / Genre,
	#[deprecated]
	/// Duplicated by [Demographic][].
	29 => JoseiDemographic("Josei") / Demographic,
	30 => ShounenAi("Shounen Ai") / Genre,
	31 => SliceOfLife("Slice of Life") / Genre,
	32 => Smut / Content,
	33 => Sports / Genre,
	34 => Supernatural / Theme,
	35 => Tragedy / Genre,
	36 => LongStrip("Long Strip") / Format,
	37 => Yaoi / Genre,
	38 => Yuri / Genre,
	#[deprecated]
	/// Deprecated due to the split regarding Genderswap and Crossdressing.
	39 => GenderBenderTheme("Gender Bender") / Theme,
	40 => VideoGames("Video Games") / Theme,
	41 => Isekai / Genre,
	42 => Adaptation / Format,
	43 => Anthology / Format,
	44 => WebComic("Web Comic") / Format,
	45 => FullColor("Full Colour") / Format,
	46 => UserCreated("User Created") / Format,
	47 => OfficialColored("Official Coloured") / Format,
	48 => FanColored("Fan Coloured") / Format,
	49 => Gore / Content,
	50 => SexualViolence("Sexual Violence") / Content,
	51 => Crime / Genre,
	52 => MagicalGirls("Magical Girls") / Genre,
	53 => Philosophical / Genre,
	54 => Superhero / Genre,
	55 => Thriller / Genre,
	56 => Wuxia / Genre,
	57 => Aliens / Theme,
	58 => Animals / Theme,
	59 => Crossdressing / Theme,
	60 => Demons / Theme,
	61 => Delinquents / Theme,
	62 => Genderswap / Theme,
	63 => Ghosts / Theme,
	64 => MonsterGirls("Monster Girls") / Theme,
	65 => Loli / Theme,
	66 => Magic / Theme,
	67 => Military / Theme,
	68 => Monsters / Theme,
	69 => Ninja / Theme,
	70 => OfficeWorkers("Office Workers") / Theme,
	71 => Police / Theme,
	72 => PostApocalyptic("Post Apocalyptic") / Theme,
	73 => Reincarnation / Theme,
	74 => ReverseHarem("Reverse Harem") / Theme,
	75 => Samurai / Theme,
	76 => Shota / Theme,
	77 => Survival / Theme,
	78 => TimeTravel("Time Travel") / Theme,
	79 => Vampires / Theme,
	80 => TraditionalGames("Traditional Games") / Theme,
	81 => VirtualReality("Virtual Reality") / Theme,
	82 => Zombies / Theme,
	83 => Incest / Theme,
	84 => Mafia / Theme,
	85 => Villainess / Theme,
}

impl From<u8> for Genre {
	fn from(v: u8) -> Genre {
		if v <= NUM_GENRES {
			unsafe { mem::transmute(v) }
		} else {
			Genre::Unknown
		}
	}
}

crate::make_set_type!(GenreSet(u128) / Genre + NUM_GENRES);
impl fmt::Debug for GenreSet {
	fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
		fmt
			.debug_set()
			.entries((0..=NUM_GENRES).filter_map(|v| {
				let genre = Genre::from(v);
				if genre.category() == Category::Demographic {
					None
				} else if self.has(genre) {
					Some(genre)
				} else {
					None
				}
			}))
			.finish()
	}
}
#[derive(
	Deserialize_repr,
	Serialize_repr,
	Eq,
	Ord,
	PartialEq,
	PartialOrd,
	Clone,
	Copy,
	Debug,
)]
#[cfg_attr(feature = "derive_more", derive(derive_more::Display))]
#[repr(u8)]
#[non_exhaustive]
pub enum Demographic {
	Shounen = 1,
	Shoujo = 2,
	Seinen = 3,
	Josei = 4,
	#[serde(other)]
	None = 0,
}
impl From<Demographic> for Genre {
	fn from(demo: Demographic) -> Self {
		use Demographic::*;
		use Genre::*;
		match demo {
			Shounen => ShounenDemographic,
			Shoujo => ShoujoDemographic,
			Seinen => SeinenDemographic,
			Josei => JoseiDemographic,
			None => Unknown,
		}
	}
}

#[cfg(test)]
pub mod tests {
	use super::*;
	#[test]
	fn genre_produces_unknown_from_0() {
		let genre = Genre::from(0);
		assert_eq!(genre, Genre::Unknown);
	}
	#[test]
	fn new_genre_set_is_empty() {
		let genres = GenreSet::new();
		assert_eq!(genres.0, 0);
		let set_tx = format!("{:?}", genres);
		assert_eq!(set_tx, "{}");
		for i in 0..=NUM_GENRES {
			assert!(!genres.has(i));
		}
	}
	#[test]
	fn full_genre_set_contains_all() {
		let genres = GenreSet(u128::max_value() & !1);
		for i in 1..=NUM_GENRES {
			assert!(genres.has(i));
		}
		eprintln!("{:?}", genres);
	}
	#[test]
	fn part_genre_set_is_contained_by_full() {
		let genres = GenreSet(u128::max_value());
		let less_genres = GenreSet(u128::max_value() & !1);
		assert!(genres != less_genres);
		assert!(less_genres.contained(genres));
	}
	#[test]
	fn part_genre_set_contains_other() {
		let genres = GenreSet(u128::max_value());
		let less_genres = GenreSet(u128::max_value() & !1);
		assert!(genres != less_genres);
		assert!(genres.contains(less_genres));
	}
	#[test]
	fn genre_set_from_vector_of_genres() {
		use Genre::*;
		let genres: GenreSet = vec![FourKoma, Action, Adventure].into();
		assert_eq!(genres, GenreSet(14));
		eprintln!("{:?}", genres);
	}
	#[test]
	fn genre_set_from_vector_of_u8() {
		let genres: GenreSet = vec![1, 2, 3].into();
		assert_eq!(genres, GenreSet(14));
		eprintln!("{:?}", genres);
	}
}
