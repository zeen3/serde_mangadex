//! A map type that takes stringified ints
//!
//! This is a (de)serializable map type that takes ID int
//! values that have value.
//!
//! While internal implementation uses a btree map, do
//! not rely on this being ordered.

use crate::maybe_id::{IDHasVal, TakeID, WithID};
use std::collections::BTreeMap as Map;
use std::fmt;
use std::iter::{Extend, FromIterator};
use std::num::NonZeroU32;

/// A stringish-integer map.
#[derive(Clone, PartialEq)]
pub struct IntMap<V, K = NonZeroU32>(pub Map<K, V>)
where
	K: IDHasVal;

impl<V: WithID<K> + Clone, K: IDHasVal> fmt::Debug for IntMap<V, K>
where
	<V as WithID<K>>::Output: fmt::Debug,
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.debug_set()
			.entries(self.0.iter().map(|(k, v)| v.clone().with_id(*k)))
			.finish()
	}
}

impl<V, K: IDHasVal> Default for IntMap<V, K> {
	fn default() -> Self {
		Self(Map::new())
	}
}
impl<V, K: IDHasVal> IntoIterator for IntMap<V, K> {
	type Item = (K, V);
	type IntoIter = ::std::collections::btree_map::IntoIter<K, V>;
	fn into_iter(self) -> Self::IntoIter {
		self.0.into_iter()
	}
}
impl<V: TakeID> FromIterator<V> for IntMap<V::Output, V::ID> {
	fn from_iter<I>(iter: I) -> Self
	where
		I: IntoIterator<Item = V>,
	{
		Self(Map::from_iter(iter.into_iter().map(V::take_id)))
	}
}
impl<V: TakeID> Extend<V> for IntMap<V::Output, V::ID> {
	fn extend<I>(&mut self, iter: I)
	where
		I: IntoIterator<Item = V>,
	{
		let iter = iter.into_iter().map(TakeID::take_id);
		self.extend(iter)
	}
}
impl<V, K: IDHasVal> Extend<(K, V)> for IntMap<V, K> {
	fn extend<I>(&mut self, iter: I)
	where
		I: IntoIterator<Item = (K, V)>,
	{
		self.0.extend(iter)
	}
}

mod serde_support {
	use super::*;
	use serde::{de::*, ser::*};
	use std::{fmt, str::FromStr};
	struct IVisitor<V, K: IDHasVal>(std::marker::PhantomData<*const (V, K)>);

	impl<V, K: IDHasVal> IVisitor<V, K> {
		fn new() -> Self {
			Self(std::marker::PhantomData)
		}
	}

	impl<'de, V, K> Deserialize<'de> for IntMap<V, K>
	where
		K: Deserialize<'de> + IDHasVal + FromStr,
		<K as FromStr>::Err: fmt::Display,
		V: Deserialize<'de>,
	{
		fn deserialize<D: Deserializer<'de>>(d: D) -> Result<Self, D::Error> {
			d.deserialize_map(IVisitor::new())
		}
	}

	#[derive(Debug, serde::Deserialize)]
	#[serde(untagged)]
	enum StrKey<'a, K: IDHasVal + FromStr> {
		Str(&'a str),
		Key(K),
	}
	impl<'de, V, K> Visitor<'de> for IVisitor<V, K>
	where
		K: IDHasVal + Deserialize<'de> + FromStr,
		<K as FromStr>::Err: fmt::Display,
		V: Deserialize<'de>,
	{
		type Value = IntMap<V, K>;
		fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
			f.write_str("a map of possibly stringified ints to values")
		}
		fn visit_map<M: MapAccess<'de>>(
			self,
			mut m: M,
		) -> Result<Self::Value, M::Error> {
			let mut g = Map::new();
			while let Some(kv) = m.next_entry()? {
				let (k, v): (StrKey<K>, V) = kv;
				let k = match k {
					StrKey::Key(k) => k,
					StrKey::Str(k) => k.parse().map_err(M::Error::custom)?,
				};
				g.insert(k, v);
			}

			Ok(IntMap(g))
		}
	}

	impl<V: Serialize, K: IDHasVal + Serialize + ToString> Serialize
		for IntMap<V, K>
	{
		fn serialize<S: Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
			if s.is_human_readable() {
				let human = self.0.iter().map(|(k, v)| (k.to_string(), v));
				s.collect_map(human)
			} else {
				s.collect_map(&self.0)
			}
		}
	}
}
