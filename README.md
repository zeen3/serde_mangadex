# serde-mangadex

json from mangadex

[![pipeline status](https://gitlab.com/zeen3/serde_mangadex/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/zeen3/serde_mangadex/commits/master) \| [![coverage report](https://gitlab.com/zeen3/serde_mangadex/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/zeen3/serde_mangadex/commits/master)

produces information

includes a genericified manga info retriever

notice: this does not include a runtime, only the mangadex api front
